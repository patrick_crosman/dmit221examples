package ca.nait.dmit.domain;

/**
 * When you contact lending institutions about acquiring a mortgage, 
 * the load officers provide you with guidelines for computing your maximum monthly mortgage payment. 
 * Common "rules of thumb" are that your maximum monthly payment should not exceed:
 * 
 * 28% of your gross monthly income
 * or
 * 36% of your gross monthly income minus monthly debit payments (loans, credit cards, etc)
 * 
 * where the mortgage payment includes principal, interest, taxes, and insurance.
 * 
 * This class is used to calculate the maximum monthly payment 
 * for a mortgage when given the monthly income and the monthly debt.
 * @author Sam Wu
 * @version 2007.01.15
 */
public class HouseRules {

	/** Constant used to calculate the monthly payment without debt */
	private final double WITHOUT_DEBT = 0.28;
	/** Constant used to calcualte the monthly payment with debt */
	private final double WITH_DEBT = 0.36;
	private double monthlyIncome;
	private double monthlyDebt;
	
	public HouseRules() {
		monthlyIncome = 0;
		monthlyDebt = 0;
	}
	
	public HouseRules(double monthlyIncome, double monthlyDebt) {
		super();
		this.monthlyIncome = monthlyIncome;
		this.monthlyDebt = monthlyDebt;
	}

	/**
	 * This method calculates the maximum monthly payment with debt. 
	 * @return The monthly payment amount.
	 */
	public double getMonthlyPaymentWithDebt() {
		return ( WITH_DEBT * monthlyIncome ) - monthlyDebt;
	}
	/**
	 * This method calculates the maximum monthly payment without debt.
	 * @return The monthly payment amount.
	 */
	public double getMonthlyPaymentWithoutDebt() {
		return WITHOUT_DEBT * monthlyIncome;
	}
	
	public double getMonthlyDebt() {
		return monthlyDebt;
	}
	public void setMonthlyDebt(double monthlyDebt) {
		this.monthlyDebt = monthlyDebt;
	}
	public double getMonthlyIncome() {
		return monthlyIncome;
	}
	public void setMonthlyIncome(double monthlyIncome) {
		this.monthlyIncome = monthlyIncome;
	}
}







