package ca.nait.dmit.controller;

import java.text.NumberFormat;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import util.JSFUtil;
import ca.nait.dmit.domain.HouseRules;

@ManagedBean(name="houseRulesBean")
@ViewScoped
public class HouseRulesController {

	private HouseRules houseRules = new HouseRules();

	public HouseRules getHouseRules() {
		return houseRules;
	}

	public void setHouseRules(HouseRules houseRules) {
		this.houseRules = houseRules;
	}
	
	public void submit() {
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
		JSFUtil.addInfoMessage("Your maximum monthly payment with debt payments is " 
				+ numberFormat.format( houseRules.getMonthlyPaymentWithDebt() ) ) ;
		JSFUtil.addInfoMessage("Your maximum monthly payment without debt payments is " 
				+ numberFormat.format( houseRules.getMonthlyPaymentWithoutDebt() ) );
	}
	
	public void clear() {
		houseRules = new HouseRules();
	}
}
