package ca.nait.dmit.controller;
import java.text.NumberFormat;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;


import util.JSFUtil;
import util.Loan;
import util.LoanSchedule;

@ManagedBean(name = "loanBean")
@ViewScoped

/**
 * Use this class as a controller to create loan schedules for a JSF page
 * @author patrickcrosman
 *
 */
public class LoanController {
	private Loan loan = new Loan();
	
	LoanSchedule[] loanScheduleTable;
	
	public LoanSchedule[] getLoanScheduleTable() {
		return loanScheduleTable;
	}

	public void setLoanScheduleTable(LoanSchedule[] loanScheduleTable) {
		this.loanScheduleTable = loanScheduleTable;
	}

	public Loan getloan()
	{
		return loan;		
	}
	
	public void setLoan(Loan loan)
	{
		this.loan = loan;
	}
	
	/***
	 * use this method to display a monthly payment to the user
	 */
	public void calculatePayment()
	{
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		JSFUtil.addInfoMessage(
				"Your Monthly Payment is: "
				+formatter.format(loan.getMonthlyPayment())
				);
		loanScheduleTable = loan.getLoanScheduleArray();
	}
}
