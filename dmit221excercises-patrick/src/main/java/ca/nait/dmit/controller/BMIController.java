package ca.nait.dmit.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import util.JSFUtil;
import util.BMI;

@ManagedBean(name = "bmiBean")
@RequestScoped
public class BMIController {
	private BMI bmi = new BMI();
			
	public BMI getBmi() {
		return bmi;
	}



	public void setBmi(BMI bmi) {
		this.bmi = bmi;
	}

	public void calculateBMI()
	{
		JSFUtil.addInfoMessage(
				"Your BMI is: "
				+bmi.getBMI() + ". You are categorized as " + bmi.getBMICategory()
				);
	}
}
