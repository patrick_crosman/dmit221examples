package util;



public class Loan {
	
	private double mortgageAmount;
	private double annualInterestRate;
	private int amPeriod;
	
	
	
	 public double getMortgageAmount() {
		return mortgageAmount;
	}


	public void setMortgageAmount(double mortgageAmount) {
		this.mortgageAmount = mortgageAmount;
	}


	public double getAnnualInterestRate() {
		return annualInterestRate;
	}


	public void setAnnualInterestRate(double annualInterestRate) {
		this.annualInterestRate = annualInterestRate;
	}


	public int getAmPeriod() {
		return amPeriod;
	}


	public void setAmPeriod(int amPeriod) {
		this.amPeriod = amPeriod;
	}


	public Loan() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Loan(double mortgageAmount, double annualInterestRate,
			int amPeriod) {
		super();
		this.mortgageAmount = mortgageAmount;
		this.annualInterestRate = annualInterestRate;
		this.amPeriod = amPeriod;
	}
	
	/**
	   * use this class to calculate how much a person needs to pay per month
	   * @return the value 
	   */
	
	public double getMonthlyPayment()
	{
		double monthlyPayment;
		
		monthlyPayment = mortgageAmount *(((Math.pow(1+(annualInterestRate / 200), (1.0/6)))-1.0) / (1-(Math.pow(Math.pow(1+(annualInterestRate/200), (1.0/6)), (-12*amPeriod)))));
		
		monthlyPayment = roundTo2Decimals(monthlyPayment);
		
		return monthlyPayment;
	}
	
	/**
	   * this class gets the schedule of loan payment
	   * @return the loan schedule
	   */
	
	public LoanSchedule[] getLoanScheduleArray()
	{
		// number of payments is the loan term (in years) multiply by the number of months per year (12)
		int numberOfPayments = amPeriod * 12;
		//System.out.println(numberOfPayments);
		LoanSchedule[] loanScheduleArray = new LoanSchedule[numberOfPayments];
		LoanSchedule loanSchedule = new LoanSchedule();
		// set the initial remaining balance is equal to amount borrowed
		// calculate monthlyPercentageRate
		
		loanSchedule.setRemainingBalance(mortgageAmount);
		double mpr = Math.pow((1+(annualInterestRate/200.0)), (1.0/6.0))-1;
		double remBalance = mortgageAmount;
		//System.out.println(mpr);
		for( int paymentNumber = 1; paymentNumber <= numberOfPayments;  paymentNumber++ )
		{
			double interestPaid;
			double principalPaid;
			double remainingBalance;
			//double hold = loanSchedule.getRemainingBalance();
			
			//System.out.println(mpr);
			//System.out.println(loanSchedule.getRemainingBalance());
			// calculate interestPaid and round to 2 decimal places
			
			interestPaid = mpr * remBalance;
			
			//System.out.println(interestPaid);
			
			interestPaid = roundTo2Decimals(interestPaid);
			
			// calculate principalPaid and round to 2 decimal places
			
			principalPaid = getMonthlyPayment() - interestPaid;
			principalPaid = roundTo2Decimals(principalPaid);
			
			if(remBalance < getMonthlyPayment())
			{
				principalPaid = remBalance;				
			}
			principalPaid = roundTo2Decimals(principalPaid);
			// update remainingBalance and round to 2 decimal places
			remainingBalance =  remBalance - principalPaid;
			remBalance = remainingBalance;
			remainingBalance = roundTo2Decimals(remainingBalance);
			// set remainingBalance to zero if it is calculated to be less than zero
			
			// arrays in Java are 0-index based
			loanScheduleArray[ paymentNumber -1 ] = new LoanSchedule( paymentNumber, interestPaid, principalPaid, remainingBalance );
		}
		return loanScheduleArray;
	}				


	/**
	   * Rounds a double value to 2 decimal places
	   * @param valueToRound the value to round
	   * @return the value rounded to 2 decimal places
	   */
	  public static double roundTo2Decimals(double valueToRound)
	  {
	  	return Math.round( valueToRound * 100 ) / 100.0;
	  }
}

