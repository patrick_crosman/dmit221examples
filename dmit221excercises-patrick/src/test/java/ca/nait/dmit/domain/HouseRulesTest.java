package ca.nait.dmit.domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class HouseRulesTest {

	@Test
	public void testLowDebts() {
		HouseRules houseRules1 = new HouseRules(2000,100);
		assertEquals(560, houseRules1.getMonthlyPaymentWithoutDebt(), 0.005);
		assertEquals(620, houseRules1.getMonthlyPaymentWithDebt(), 0.005);
	}

	@Test
	public void testHighDebts() {
		HouseRules houseRules1 = new HouseRules(3000,500);
		assertEquals(840, houseRules1.getMonthlyPaymentWithoutDebt(), 0.005);
		assertEquals(580, houseRules1.getMonthlyPaymentWithDebt(), 0.005);
	}

	@Test
	public void testNoDebts() {
		HouseRules houseRules1 = new HouseRules(1500,0);
		assertEquals(420, houseRules1.getMonthlyPaymentWithoutDebt(), 0.005);
		assertEquals(540, houseRules1.getMonthlyPaymentWithDebt(), 0.005);
	}

}
